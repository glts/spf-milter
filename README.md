# SPF Milter

The SPF Milter project has moved to the [Codeberg] platform:

<https://codeberg.org/glts/spf-milter>

Please update your links.

[Codeberg]: https://codeberg.org
